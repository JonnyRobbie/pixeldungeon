# Pixel Dungeon damage stat calculator

This utility is used to visualize damage stats and probabilities of a player/mob interactions.

## Usage

### Online

  Go to [jonnyrobbie.shinyapps.io/pixeldungeon](https://jonnyrobbie.shinyapps.io/pixeldungeon/)
  
  I only have a free limited account, so try not to over extert my compute time quota. If it is possible, try to run this offline.
  
### Offline
  
  From system shell:

    git clone https://gitlab.com/JonnyRobbie/pixeldungeon.git

  From R shell (make sure your working directory is the repo directory):
  
    library(shiny)
    setwd("pixeldungeon/")
    runApp(".")

## Help

 * **Name** - for display purposes.
 * **Strength** - character strength.
 * **Level** *(in Player stats tab)* - character level. Is used to calculate *Accuracy* and *Dodge* in the player tab.
 * **Accuracy** *(in Enemy stats tab)* - character accuracy.
 * **Dodge** *(in Enemy stats tab)* - character dodge.
 * **Weapons accuracy** - space-separated list of all character's accuracy modifiers. Equipped weapon goes first. When using magis, this should be manually set to 2.
 * **Armors dodge** - space-separated list of all character's dodge modifiers. Equipped armor goes first.
 * **Weapon min strength** - strength requirement for equipped weapon.
 * **Armor min strength** - strength requirement for equipped armor.
 * **Minimum hit** - lower bound on equipped weapon's attack.
 * **Maximum hit** - upper bound on equipped weapon's attack.
 * **Minimum armor** - lower bound on equipped armor's defense.
 * **Maximum armor** - upper bound on equipped armor's defense.
 
## Bestiary

There is currently only a limited amount of predefined mobs in bestiary. I would greatly appreciate any patches or pull requests on `bestiary.R` file.

## Screenshot

![screenshot](https://gitlab.com/JonnyRobbie/pixeldungeon/raw/master/screenshot.png)
